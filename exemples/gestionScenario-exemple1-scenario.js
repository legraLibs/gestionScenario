/*!
fichier: exemple1-scenario.js
auteur:pascal TOLEDO
date: 2012.02.23
source:
depend de:
description:
*/

scenario = new Tscenario();

//------ definition des scenes ----
/////////////////////////////////////////////
scene1= function()//mise en place
	{
	addText('la scene:'+this.nom+' dure '+this.duree+' secondes','');
	}

//CallBack de debut
function scene_CBdeb(){testId.innerHTML='clear';addText('<span class="CBDeb">debut:</span>','');}

//CallBack de vie (a chaque iteration)
function bip(){addText('-','');}

//CallBack de fin
function scene1_CBfin()
	{
	addText('<span class="CBFin">fin:</span> nombre d\'iteration:'+scenario.scenes[1].iterationVie);
	}

/////////////////////////////////////////////
scenario.scene2= function(){addText('la scene:'+this.nom+' dure '+this.duree+' secondes','');}
scenario.scene3= function(){addText('la scene:'+this.nom+' dure '+this.duree+' secondes');}
//------ ajout des scenes (dans l'ordre d'execution)----
scenario.addScene(
	{
	duree:5
	,nom:'scene1'
	,CBdeb:scene_CBdeb
//	,CBvie:bip
//	,CBvie:function(){addText('*'+scenario.scene_info());}
	,CBvie:function(){addText('<span class="CBVie">*'+scenario.scene_info(1)+'</span>');}
	,sceneCode:scene1
	,CBfin:scene1_CBfin
	});

scenario.addScene(
	{
	duree:7
	,nom:'scene2'
	,CBdeb:function(){addText('<span class="CBDeb">D2</span>','');}
	,CBvie:function(){addText('<span class="CBVie">.</span>','');}
	,sceneCode:scenario.scene2
	,CBfin:function(){addText('<span class="CBFin">FIN</span>','<br>');}
	});

scenario.addScene(
	{
	duree:3
	,nom:'scene3'
	,CBdeb:function(){addText('<span class="CBDeb">D3</span>','');}
	,sceneCode:function(){addText('la scene3 dure '+this.duree+' secondes','');}
	,CBvie:function(){addText('<span class="CBVie">=</span>','');}
	,CBfin:function(){addText('<span class="CBFin"><br>***************************************</span>','<br>');}
	});

//------ Execution -----------------
//scenario.runAuto('scenario');
